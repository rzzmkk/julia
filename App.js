import React from 'react';
import {createStackNavigator, createAppContainer} from 'react-navigation';
import FirstPage from './Components/FirstPage';
import SecondPage from './Components/SecondPage';
import ThirdPage from './Components/ThirdPage';
import Barcode from './Components/Barcode';
import ManualInput from './Components/ManualInput';
import BarcodeHandler from './Components/BarcodeHandler';
import SellProduct from './Components/SellProduct';
import MainScreen from './Components/MainScreen';
import BarcodeSold from './Components/BarcodeSold';
import Login from './Components/Login';

class App extends React.Component {
  render() {
    return (<AppStackNavigator/>)
  }
}

const AppStackNavigator = createStackNavigator({
  FirstPage: {
    screen: FirstPage
  },
  SecondPage: {
    screen: SecondPage
  },
  ThirdPage: {
    screen: ThirdPage
  },
  MainScreen: {
    screen: MainScreen
  },
  SellProduct: {
    screen: SellProduct
  },
  BarcodeSold: {
    screen: BarcodeSold
  },
  Barcode: {
    screen: Barcode
  },
  BarcodeHandler: {
    screen: BarcodeHandler
  },
  ManualInput: {
    screen: ManualInput
  },
  Login: {
    screen: Login
  }
}, {initialRouteName: 'FirstPage'})

export default createAppContainer(AppStackNavigator);
