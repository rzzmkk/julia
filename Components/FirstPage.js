import React from 'react';
import {
  Text,
  View,
  Button,
  TextInput,
  Keyboard,
  TouchableWithoutFeedback
} from 'react-native';

export default class FirstPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      text: '',
    }
  }
  state = {
    uuid: null
  }

  getCode(text) {
    let phone = text;
    fetch(`https://sandbox.aviago.app/api/v1/mainModule/authentication/${phone}/partner/`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        type: 'partner',
      }
      })
      .then(response => response.json())
      .then(response => {
        console.log(response);
        if (response['detail']=='not found' || response.status==404) {
          fetch(`https://sandbox.aviago.app/api/v1/mainModule/authentication/${phone}/sms/`,{
            method: 'GET',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
              type: 'partner',
            }
          }).then(response => {
            console.log("sms " + response)
            let uuid = response['data']['id'];
            this.setState({uuid});
          });
          let id = this.state.uuid;
          this
            .props
            .navigation
            .navigate('SecondPage', {id, text});
        }
        else if (response.status == 200){
          console.log('User registered')
        }
      })
  };

  render() {
    return (
      <View
        style={{
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
      }}>
        <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
          <View
            style={{
            width: 200,
            height: 60,
            borderColor: 'gray',
            borderWidth: 1,
            justifyContent: "center",
            alignItems: "center"
          }}>
            <View>
              <Text>Введите номер телефона ниже
              </Text>
            </View>
            <View>
              <TextInput
                keyboardType='phone-pad'
                onChangeText={text => {
                  this.setState({text})
                }
              }
                value={this.state.text}
                maxLength={10}
                placeholder={"Ваш номер телефона"}/>
            </View>
          </View>
        </TouchableWithoutFeedback>
        <Button title="Получить код" onPress= {() => this.getCode(this.state.text)}/>
      </View>
    );
  }
}
