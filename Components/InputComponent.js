/* @flow */

import * as React from 'react';
import {
  StyleSheet,
  View,
  ScrollView,
  KeyboardAvoidingView,
  type Theme,
} from 'react-native';
import { TextInput, HelperText, withTheme } from 'react-native-paper';

type Props = {
  theme: Theme,
};

type State = {
  text: string,
  name: string,
  entity: string,
  bin: string,
  outlinedText: string,
  shopName: string,
};

class InputComponent extends React.Component<Props, State> {
  static title = 'TextInput';

  state = {
    text: '',
    entity: '',
    bin: '',
    name: '',
    shopName: '',
  };

  _isUsernameValid = () => /^[a-zA-Z]*$/.test(this.state.name);
  _isBINValid = () => /^(\d{9})?$/.test(this.state.bin);

  render() {
    const {
      theme: {
        colors: { background },
      },
    } = this.props;

    return (
      <KeyboardAvoidingView
        style={styles.wrapper}
        behavior="padding"
        keyboardVerticalOffset={80}
      >
        <ScrollView
          style={[styles.container, { backgroundColor: background }]}
          keyboardShouldPersistTaps={'always'}
          removeClippedSubviews={false}
        >
          <View style={styles.inputContainerStyle}>
            <TextInput
              label="Введите БИН"
              placeholder="Введите БИН"
              keyboardType='phone-pad'
              value={this.state.bin}
              error={!this._isUsernameValid()}
              onChangeText={bin => this.setState({ bin })}
            />
            <HelperText type="error" visible={!this._isBINValid()}>
                Только 9 цифр
            </HelperText>
          </View>
          <TextInput
            mode="outlined"
            style={styles.inputContainerStyle}
            label="Название юр. лица"
            placeholder="Название юр. лица"
            value={this.state.entity}
            onChangeText={entity => this.setState({ entity })}
          />
          <TextInput
            mode="outlined"
            style={styles.inputContainerStyle}
            label="Название магазина"
            placeholder="Название магазина"
            value={this.state.shopName}
            onChangeText={shopName => this.setState({ shopName })}
          />
        </ScrollView>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 8,
  },
  wrapper: {
    flex: 1,
  },
  inputContainerStyle: {
    margin: 8,
  },
});

export default withTheme(InputComponent);