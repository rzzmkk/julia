import * as React from 'react';
import {View, StyleSheet, TouchableOpacity} from 'react-native';
import {Colors, Button, withTheme, type Theme} from 'react-native-paper';

type Props = {
  theme: Theme
};

type State = {
  checked: 'too' | 'ip',
  text: string,
  name: string,
  entity: string,
  bin: string,
  outlinedText: string,
  shopName: string
};

class MainScreen extends React.Component < Props,
State > {
  constructor(props) {
    super(props);
  }
  static title = 'Radio Button';

  getCode(uuid, code, bin, name) {
    const body = new FormData;
    body.append("authentication", uuid);
    body.append("bin", bin);
    body.append("code", code);
    body.append("name", name);

    fetch('http://staging.aviago.app/api/v1/mainModule/partner/', {
      method: 'POST',
      body,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Token: "2f30743e-014d-4f0f-8707-73ae550d8f14"
      }
    }).then(response => {
      console.log(response);
      this
        .props
        .navigation
        .navigate('Barcode');
    })
  };
  state = {
    checked: null,
    entity: '',
    bin: '',
    name: '',
    shopName: ''
  };

  _isBINValid = () => /^(?:\d{9})?$/.test(this.state.bin);

  addProd = () =>{
      this.props.navigation.navigate('Barcode');
  }

  sellProd = () => {
      this.props.navigation.navigate('SellProduct');
  }

  render() {
    const {
      theme: {
        colors: {
          background
        }
      }
    } = this.props;

    return (
      <View
        style={[
        styles.container, {
          backgroundColor: background
        }
      ]}>
        <TouchableOpacity onPress={() => this.addProd()}>
          <Button style={[styles.button]} icon="add-a-photo" mode="outlined">
            Добавить товары
          </Button>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => this.sellProd()}>
          <Button
            style={[styles.button]}
            icon="add-a-photo"
            mode="contained">
            Продажа
            товаров</Button>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
    paddingVertical: 8,
    padding: 8
  },
  wrapper: {
    flex: 1
  },
  button: {
    height: 250,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: 8,
    paddingHorizontal: 16
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: 8,
    paddingHorizontal: 16
  },
  inputContainerStyle: {
    margin: 8
  },
});

export default withTheme(MainScreen);