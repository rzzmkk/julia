import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {Constants, Permissions} from 'expo';
import {withTheme} from 'react-native-paper';

class BarcodeHandler extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      barcodeCamera: this.props.navigation.state.params.dt,
    };
  }
  state = {
    hasCameraPermission: true,
    barcode: this.props.navigation.state.params.dt,
    name: null,
    scanning: true
  }

  checkProductName(data) {
    console.log('Apollon 01');
    fetch('https://sandbox.aviago.app/api/v1/marketModule/good/', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
        body: JSON.stringify({"barcode": data})
      })
      .then(response => response.json())
      .then(response => {
        console.log(response);
        // let bar = barcode;
        // this.props.navigation.navigate('BarcodeHandler', {bar});
        if (response['data']['barcode']==undefined && response['data']['name']=="Это поле обязательно."){
          this.getProductName(data);
        }

      })
      .catch(function (error) {
        console.log(error.message);
      })
  }
  //olegon
  getProductName(data) {
    console.log('Olegon');
    fetch(`https://barcodes.olegon.ru/api/card/name/${data}/B783984834510711938732096294968`, {method: 'POST'})
      .then(response => response.json())
      .then(response => {
        console.log(response);
        if (response.status == 404 || response['status']==404) {
          this
            .props
            .navigation
            .navigate('ManualInput', {data});
        } else {
          let name = response['names'][0];
          this.setState({barcode: data});
          this.setState({name})
          this.saveProductName(this.state.barcode, this.state.name);
        }
      })
      .catch(function (error) {
        console.log('error is here ' + error.message);
      })
  };

  saveProductName(barcode, name) {
    console.log('Apollon 02');
    fetch('https://sandbox.aviago.app/api/v1/marketModule/good/', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
        body: JSON.stringify({"barcode": barcode, "name": name})
      })
      .then(response => response.json())
      .then(response => {
        console.log(response);
        let name = response['data']['name']
        //alert(barcode); alert(name + 'был сканирован');
        alert(`${name} был сканирован`, [
          {
            text: 'OK',
            onPress: () => this
              .props
              .navigation
              .navigate('Barcode')
          }
        ], {
          cancelable: false
        },);
      })
      .catch(function (error) {
        console.log(error.message);
      })
  }

  async componentDidMount() {
    const {status} = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({
      hasCameraPermission: status === 'granted'
    });
  }

  componentWillMount(){
    this.checkProductName(this.state.barcodeCamera);
  }

  render() {
    return (
      <View style={styles.container}>
        <Text>Barcode Handler</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingVertical: 8,
    padding: 8,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: Constants.statusBarHeight,
    backgroundColor: '#ecf0f1'
  },
  wrapper: {
    flex: 1
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: 8,
    paddingHorizontal: 16,
    margin: 8
  },
  inputContainerStyle: {
    margin: 8
  }
});

export default withTheme(BarcodeHandler);