import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {Constants,} from 'expo';
import {withTheme} from 'react-native-paper';

class BarcodeHandler extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      barcodeCamera: this.props.navigation.state.params.dt,
    };
  }
  state = {
    hasCameraPermission: true,
    barcode: this.props.navigation.state.params.dt,
    name: null,
    scanning: true
  }

  sellProduct(data){
    console.log('Apollon sell');
    let ddt = data;
    fetch(`https://sandbox.aviago.app/api/v1/marketModule/good?barcode=${ddt}`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      }).then(response => response.json())
      .then(response => {
        let name = response['data']['name'];
        let error = response['data']['detail'];
        // let bar = barcode;
        // this.props.navigation.navigate('BarcodeHandler', {bar});
        console.log(response);
        console.log(response.status);
        console.log(response.ok);
        console.log(response['data']);
        console.log(response['data'][4]);
        if (response.ok || response.status==200){
          alert(`${name} был списан`);
        }
        else {
            alert(`There is an error: ${error}`);
        }
      })
      .catch(function (error) {
        console.log(error.message);
      })
  }

  componentWillMount(){
    this.sellProduct(this.state.barcodeCamera);
  }

  render() {
    return (
      <View style={styles.container}>
        <Text>Barcode sell</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingVertical: 8,
    padding: 8,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: Constants.statusBarHeight,
    backgroundColor: '#ecf0f1'
  },
  wrapper: {
    flex: 1
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: 8,
    paddingHorizontal: 16,
    margin: 8
  },
  inputContainerStyle: {
    margin: 8
  }
});

export default withTheme(BarcodeHandler);