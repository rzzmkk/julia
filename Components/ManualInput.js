import React from 'react';
import {StyleSheet, Text, Button, View} from 'react-native';
import {TextInput, withTheme,  Colors} from 'react-native-paper';

class ManualInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      barcodeManual: this.props.navigation.state.params.data
    }
  }
  state = {
    name: null,
    manualBarcode: null
  }

  saveProductName(barcode, name) {
    console.log('Apollon 03');
    fetch('https://sandbox.aviago.app/api/v1/marketModule/good/', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
        body: JSON.stringify({"barcode": barcode, "name": name})
      })
      .then(response => {
        if (response.ok || response.status == 200) {
          alert('Продукт сохранен');
          this
            .props
            .navigation
            .popToTop();
        } else {
          console.log(response);
          alert('Ошибка');
        }
      })
      .catch(function (error) {
        alert(error.message);
      })
  }

  render() {
    return (
      <View style={styles.row}>
        <View style={styles.inputContainerStyle}>
          <Text>Данный штрихкод: {this.state.barcodeManual} не был найден в базе.        Пожалуйста, добавьте его в ручную</Text>
          <TextInput
            label="Введите название продукта"
            placeholder="Введите название продукта"
            keyboardType='default'
            value={this.state.name}
            onChangeText={name => this.setState({name})}/>
        </View>
        <Button
          style={styles.row}
          onPress={() => this.saveProductName(this.state.barcodeManual, this.state.name)}
          title="Сохранить штрихкод и название продукта "
          color="#000000"
          accessibilityLabel="Сохранить штрихкод и название продукта"/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.orange,
    paddingVertical: 8,
    padding: 8,
    margin: 6
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: 8,
    paddingHorizontal: 16,
    margin: 10,
  },
  inputContainerStyle: {
    margin: 8
  }
});

export default withTheme(ManualInput);