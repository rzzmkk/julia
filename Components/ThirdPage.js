import * as React from 'react';
import {View, StyleSheet, Button, ScrollView} from 'react-native';
import {
  Colors,
  Paragraph,
  RadioButton,
  TouchableRipple,
  TextInput,
  HelperText,
  withTheme,
  type Theme,
} from 'react-native-paper';

type Props = {
  theme: Theme
};

type State = {
  checked: 'too' | 'ip',
  text: string,
  name: string,
  entity: string,
  bin: string,
  outlinedText: string,
  shopName: string
};

class ThirdPage extends React.Component < Props,
State > {
  constructor(props) {
    super(props);
    this.state = {
      uuid: this.props.navigation.state.params.uid,
      code: this.props.navigation.state.params.code
    }
  }
  static title = 'Radio Button';

  

  getCode(uuid, code, bin, name ) {
    const body = new FormData;
    body.append("authentication",uuid);
    body.append("bin", bin);
    body.append("code", code);
    body.append("name", name);

    fetch('http://staging.aviago.app/api/v1/mainModule/partner/', {
      method: 'POST',
      body,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Token: "2f30743e-014d-4f0f-8707-73ae550d8f14",
      },
      })
      .then(response => {
        console.log(response);
        this.props.navigation.navigate('Barcode');
      })
  };
  state = {
    checked: null,
    entity: '',
    bin: '',
    name: '',
    shopName: ''
  };

  _isBINValid = () =>/^(?:\d{9})?$/.test(this.state.bin);

  render() {
    const {
      theme: {
        colors: {
          background
        }
      }
    } = this.props;

    return (
      <View
        style={[
        styles.container, {
          backgroundColor: background
        }
      ]}>
        <TouchableRipple onPress={() => this.setState({checked: 'too'})}>
          <View style={styles.row}>
            <Paragraph>ТОО</Paragraph>
            <View pointerEvents="none">
              <RadioButton
                value="too"
                status={this.state.checked === 'too'
                ? 'checked'
                : 'unchecked'}/>
            </View>
          </View>
        </TouchableRipple>
        <TouchableRipple onPress={() => this.setState({checked: 'ip'})}>
          <View style={styles.row}>
            <Paragraph>ИП</Paragraph>
            <View pointerEvents="none">
              <RadioButton
                value="ip"
                color={Colors.blue500}
                status={this.state.checked === 'ip'
                ? 'checked'
                : 'unchecked'}/>
            </View>
          </View>
        </TouchableRipple>
        <ScrollView
          style={[
          styles.container, {
            backgroundColor: background
          }
        ]}
          keyboardShouldPersistTaps ={'always'}
          removeClippedSubviews={false}>
          <View style={styles.inputContainerStyle}>
            <TextInput
              label="Введите БИН"
              placeholder="Введите БИН"
              keyboardType='phone-pad'
              value={this.state.bin}
              error={!this._isBINValid()}
              onChangeText={bin => this.setState({bin})}/>
            <HelperText type="error" visible={!this._isBINValid()}>
              Только 9 цифр
            </HelperText>
          </View>
          <TextInput
            mode="outlined"
            style={styles.inputContainerStyle}
            label="Название юр. лица"
            placeholder="Название юр. лица"
            value={this.state.entity}
            onChangeText={entity => this.setState({entity})}/>
          <TextInput
            mode="outlined"
            style={styles.inputContainerStyle}
            label="Название магазина"
            placeholder="Название магазина"
            value={this.state.shopName}
            onChangeText={shopName => this.setState({shopName})}/>
        </ScrollView>
        <Button
          title="Click me CYKA"
          onPress={() => this.getCode(this.state.uuid, this.state.code, this.state.bin, this.state.shopName)}/>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
    paddingVertical: 8,
    padding: 8
  },

  wrapper: {
    flex: 1
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: 8,
    paddingHorizontal: 16
  },
  inputContainerStyle: {
    margin: 8
  }
});

export default withTheme(ThirdPage);