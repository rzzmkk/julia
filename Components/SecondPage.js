import React, {Component} from "react";
import {View, Button} from "react-native";
import CodeInput from 'react-native-confirmation-code-input'

export default class SecondPage extends Component {
  input = React.createRef();
  constructor(props) {
    super(props);

    this.state = {
      value: "",
      focused: false,
      text: "",
      uuid: this.props.navigation.state.params.uuid,
      phone: this.props.navigation.state.params.text,
      codeConf: ''
    };
  }

  sendCode = (uid, code, phone) => {
    const body = new FormData
    body.append("authentication", uid)
    body.append("code", code)
    body.append("phone", phone)
    fetch('https://sandbox.aviago.app/api/v1/mainModule/partner/', {
        method: 'POST',
        body,
      }).then( response => {
        console.log(response);
        if (response.ok){

        }
      })
      .catch((error) => {
        console.error(error);
      });
  }

  render() {
    return (
      <View
        style={{
        flex: 1,
        backgroundColor: '#FDD7E4',
        alignSelf: 'stretch',
        textAlign: 'center',
        borderWidth: 1,
        justifyContent: "center",
        alignItems: "center"
      }}>

        <CodeInput
          ref="codeInputRef2"
          secureTextEntry
          codeLength={4}
          keyboardType='phone-pad'
          activeColor='rgba(0, 0, 0, 1)'
          inactiveColor='rgba(0, 0, 0 , 0.1)'
          autoFocus={true}
          ignoreCase={true}
          inputPosition='center'
          size={50}
          onFulfill={(code) => {
          this.setState({codeConf: code})
        }}/>
        <Button
          title="Отправить код"
          onPress=
          {(code) => {this.sendCode(this.state.uuid, this.state.codeConf, this.state.phone)}}/>
      </View>
    )
  }
}