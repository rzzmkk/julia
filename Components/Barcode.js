import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {Constants, BarCodeScanner, Permissions} from 'expo';
import {withTheme} from 'react-native-paper';

class Barcode extends React.Component {
  state = {
    hasCameraPermission: true,
    barcode: null,
    name: null,
    scanning: true
  }
  /* apollon
  getProductName(data) {
    fetch(`http://46.101.252.205:4040/api/v1/marketModule/good/?barcode=${data}`, {
      method: 'POST',
      })
      .then(response => response.json())
      .then(response => {
        console.log(response);
        let name = response['data']['name'];
        alert(name);
      }).catch(function(error){
        console.log('error is here ' + error.message);
      })
  };
*/
  _handleBarCodeRead = data => {
    let dt = data['data'];
    this.props.navigation.navigate('BarcodeHandler', {dt});
    //this.checkProductName(data['data']);
  };

  async componentDidMount() {
    const {status} = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({
      hasCameraPermission: status === 'granted'
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.row}>
          {this.state.hasCameraPermission === null
            ? <Text>Requesting for camera permission</Text>
            : this.state.hasCameraPermission === false
              ? <Text>Camera permission is not granted</Text>
              : <BarCodeScanner
                torchMode="on"
                onBarCodeRead={this._handleBarCodeRead}
                style={{
                height: 300,
                width: 300
              }}/>}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingVertical: 8,
    padding: 8,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: Constants.statusBarHeight,
    backgroundColor: '#ecf0f1'
  },
  wrapper: {
    flex: 1
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: 8,
    paddingHorizontal: 16,
    margin: 8
  },
  inputContainerStyle: {
    margin: 8
  }
});

export default withTheme(Barcode);