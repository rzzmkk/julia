import React from 'react';
import {
  Text,
  View,
  Button,
  TextInput,
  Keyboard,
  TouchableWithoutFeedback
} from 'react-native';

export default class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      text: '',
      password: '',
      password2: ''
    }
  }
  state = {
    uuid: null
  }

  getCode(text, psw) {
    fetch('http://staging.aviago.app/api/v1/mainModule/authentication/', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
        body: JSON.stringify({"phone": text, "password": psw})
      })
      .then(response => response.json())
      .then(response => {
        let uuid = response['data']['id'];
        this.setState({uuid});
        this
          .props
          .navigation
          .navigate('SecondPage', {uuid});
      })
  };

  render() {

    return (
      <View
        style={{
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
      }}>
        <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
          <View
            style={{
            width: 200,
            height: 60,
            borderColor: 'gray',
            borderWidth: 1,
            justifyContent: "center",
            alignItems: "center"
          }}>
            <View>
              <Text>Введите номер телефона ниже
              </Text>
            </View>
            <View>
              <TextInput
                keyboardType='phone-pad'
                onChangeText={(text) => this.setState({text})}
                value={this.state.text}
                placeholder={"Ваш номер телефона"}/>
            </View>
          </View>
        </TouchableWithoutFeedback>
        <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
          <View
            style={{
            width: 200,
            height: 60,
            marginTop: 25,
            borderColor: 'gray',
            borderWidth: 1,
            justifyContent: "center",
            alignItems: "center"
          }}>
            <View>
              <Text>Введите пароль</Text>
            </View>
            <View>
              <TextInput
                keyboardType='phone-pad'
                secureTextEntry
                onChangeText={(password) => this.setState({password})}
                value={this.state.password}
                placeholder={"Введите пароль"}/>
            </View>
          </View>
        </TouchableWithoutFeedback>
        <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
          <View
            style={{
            width: 200,
            height: 60,
            marginTop: 25,
            borderColor: 'gray',
            borderWidth: 1,
            justifyContent: "center",
            alignItems: "center"
          }}>

            <Text>Подтвердите пароль</Text>
            <TextInput
              keyboardType='phone-pad'
              secureTextEntry
              onChangeText={(password2) => this.setState({password2})}
              value={this.state.password2}
              placeholder={"Подтвердите пароль"}/>
          </View>
        </TouchableWithoutFeedback>
        <Button
          title="Получить код"
          onPress=
          {() => this.getCode(this.state.text, this.state.password)}/>
      </View>
    );
  }
}
