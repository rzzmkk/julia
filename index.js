import React from 'react';
import {createStackNavigator, createAppContainer} from 'react-navigation';
import FirstPage from './Components/FirstPage';
import SecondPage from './Components/SecondPage';
import ThirdPage from './Components/ThirdPage';
import Barcode from './Components/Barcode';
import ManualInput from './Components/ManualInput';

class App extends React.Component {

  render() {
    return (<AppStackNavigator/>)
  }
}

const AppStackNavigator = createStackNavigator({
  FirstPage: {
    screen: FirstPage
  },
  SecondPage: {
    screen: SecondPage
  },
  ThirdPage: {
    screen: ThirdPage
  },
  Barcode: {
    screen: Barcode
  },
  ManualInput: {
    screen: ManualInput
  }
}, {
  initialRouteName: 'Barcode',
  headerMode: 'none',
  navigationOptions: {
    headerVisible: false
  }
})

export default createAppContainer(AppStackNavigator);
